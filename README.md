# Example app node js with ```gitlab-ci``` in heroku


This is a project example how to deploy app node with gitlab CI on Heroku


# Configuration initial

```bash
$ mkdir <<name-project>>
$ cd 
$ git add .

$ git commit -m "Initial commit"
$ git push -u origin master
```

# Node.js Express App

```bash
$ npm init -y
$ npm install --save express

file index.js

    const express = require("express");
    
    const app = express();
    
    app.get("/", (req, res) => {
    
    res.send({ hello: "world" });
    
    });
    
    const PORT = process.env.PORT || 5000;
    
    app.listen(PORT, function() {
    
    console.log(`App listening on port ${PORT}`);
    
    });

```

# Reference
[https://medium.com/@seulkiro/deploy-node-js-app-with-gitlab-ci-cd-214d12bfeeb5](https://medium.com/@seulkiro/deploy-node-js-app-with-gitlab-ci-cd-214d12bfeeb5)
